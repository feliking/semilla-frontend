const {generateTemplateFiles} = require('generate-template-files');

generateTemplateFiles([
    {
        option: 'Crear componente(Escriba bien el nombre, se sobrescribirá cualquier archivo existente)',
        entry: {
            folderPath: './tools/componente/',
        },
        stringReplacers: [
            {
                question: 'Nombre del componente(si tiene mas de una palabra, separar con espacios)',
                slot: '__Nombre__'
            },
            {
                question: 'Roles permitidos por el componente(usar los id de los roles, Ej: 1,5,20...)',
                slot: '__Roles__'
            }
        ],
        output: {
            path: './src/',
            overwrite: true
        },
        onComplete: (results) => {
            console.log(`Archivos generados`, results.output.files);
            console.log('Componente generado exitosamente :)')
        },
    },
]);