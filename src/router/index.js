import Vue from 'vue';
import Router from 'vue-router';
import Dashboard from '@/components/admin/Dashboard';
import AppNotFound from '@/common/layout/pages/AppNotFound';
import AppForbidden from '@/common/layout/pages/AppForbidden';
import AppError from '@/common/layout/pages/AppError';

// System
import Login from '@/components/admin/auth/Login';
import Account from '@/components/admin/account/Account';
import Usuario from '@/components/admin/usuario/Usuario';
import RolRecurso from '@/components/admin/rol-recurso/Index'
import Log from '@/components/admin/Log';

import Parametro from '@/components/admin/parametro/Index';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Dashboard',
      component: Dashboard
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/usuarios',
      name: 'Usuario',
      component: Usuario
    },
    {
      path: '/roles',
      name: 'RolRecurso',
      component: RolRecurso
    },
    {
      path: '/account',
      name: 'Account',
      component: Account
    },
    {
      path: '/parametros',
      name: 'Parametro',
      component: Parametro
    },
    {
      path: '/logs',
      name: 'Logs',
      component: Log
    },
    {
      path: '/404',
      component: AppNotFound
    },
    {
      path: '/403',
      component: AppForbidden
    },
    {
      path: '/500',
      component: AppError
    },
    {
      path: '*',
      component: AppNotFound
    }
  ]
});
